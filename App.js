import React from 'react';
import Game from "./src/components/Game";
import './App.less'

const App = () => {
  return (
    <Game/>
  )
};

export default App;
