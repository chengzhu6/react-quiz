import React from 'react';
import CreateGame from "./CreateGame";
import Guess from "./Guess";
import ShowResult from "./ShowResult";
import './Game.less'

class Game extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      answer : "",
      startGame: false
    };
    this.getAnswer = this.getAnswer.bind(this);

  }

  render() {
    return (
      <div className={"gameUI"}>
        <div className={"GuessUI"}>
          <Guess  answer={this.state.answer}/>
        </div>
        <div className={"ControllerUI"}>
          <h1>New Card</h1>
          <CreateGame setAnswer={this.getAnswer}/>
          <ShowResult show={this.state.startGame} answer={this.state.answer}/>
        </div>
      </div>

    )
  }

  getAnswer(answer) {
    this.setState({answer: answer, startGame: true});
    setTimeout(() => {
      this.setState({answer: answer, startGame: false});
    }, 3000);

  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log("new game.")
  }


}

export default Game;
