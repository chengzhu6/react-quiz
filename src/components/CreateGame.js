import React from 'react';


class CreateGame extends React.Component{
  constructor(props) {
    super(props);
    this.createGame = this.createGame.bind(this);

  }

  render() {
    return (
      <button onClick={this.createGame}>New Card</button>
    );
  }


  createGame() {
    let answer = this.getRandom();
    this.props.setAnswer(answer);
  }

  getRandom() {
    let letters = "abcdefghigklmnopqrstuvwxyz";
    let upLetters = letters.toUpperCase();
    var result = '';
    let array = [...letters, ...upLetters];
    for (let i = 0; i < 4; i++) {
      let index = Math.floor(Math.random() * Math.floor(53));
      result += array[index];
    }
    return result;
  }
}

export default CreateGame;
