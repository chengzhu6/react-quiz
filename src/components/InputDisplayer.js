import React from 'react';
import './InputDisplayer.less'

class InputDisplayer extends React.Component{
  constructor(props) {
    super(props);
  }
  render() {
    return <div className={"displayer"}>{this.props.show ? this.props.value : ""}</div>
  }
}

export default InputDisplayer;
